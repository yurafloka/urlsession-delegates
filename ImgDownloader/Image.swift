//
//  Image.swift
//  ImgDownloader
//
//  Created by Yurii on 12.05.18.
//  Copyright © 2018 Yurii. All rights reserved.
//

import Foundation

class Image {

  let name: String
  let url: URL
  let index: Int
  var downloaded = false

  init(name: String, url: URL, index: Int) {
    self.name = name
    self.url = url
    self.index = index
  }
}
