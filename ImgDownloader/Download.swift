//
//  Download.swift
//  ImgDownloader
//
//  Created by Yurii on 12.05.18.
//  Copyright © 2018 Yurii. All rights reserved.
//

import Foundation

class Download {
//
  var image: Image
  var task: URLSessionDownloadTask?
  var isDownloading = false
  var resumeData: Data?
  var progress: Float = 0
  
  init(image: Image) {
    self.image = image
  }
}
