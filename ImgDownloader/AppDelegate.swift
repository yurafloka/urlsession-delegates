//
//  AppDelegate.swift
//  ImgDownloader
//
//  Created by Yurii on 12.05.18.
//  Copyright © 2018 Yurii. All rights reserved.
//

 import UIKit
 
 @UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
 
  var backgroundSessionCompletionHandler: (() -> Void)?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    return true
   }
  
  func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
    backgroundSessionCompletionHandler = completionHandler
   }
 }
 
