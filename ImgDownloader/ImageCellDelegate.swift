//
//  ImageCellDelegate.swift
//  ImgDownloader
//
//  Created by Yurii on 12.05.18.
//  Copyright © 2018 Yurii. All rights reserved.
//

import Foundation

protocol ImageCellDelegate : class {
  
  func pauseTapped(_ cell: ImageCell)
  func resumeTapped(_ cell: ImageCell)
  func downloadTapped(_ cell: ImageCell)
}
