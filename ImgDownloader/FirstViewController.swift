//
//  FirstViewController.swift
//  ImgDownloader
//
//  Created by Yurii on 12.05.18.
//  Copyright © 2018 Yurii. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
  
  var imagesArray: [Image] = []
  
  var downloadedImagesDictionary = [Int: UIImage]()
  
  let downloadService = DownloadService()
  
  let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
  
  lazy var downloadsSession: URLSession = {
    let configuration = URLSessionConfiguration.background(withIdentifier: "Configuration")
    return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
  }()
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imagesArray = createArray()
    downloadService.downloadsSession = downloadsSession
  }
  
  private func createArray() -> [Image] {
    
    var imageArray: [Image] = []
    
    let cell0 = Image(name: "image 0",url: URL(string: "https://www.swisspearl.com/download/fileadmin/template/pdf_downloads/Press/swisspearl/Swisspearl_largesize_4.jpg")!, index: 0)
    let cell1 = Image(name: "image 1",url: URL(string: "https://upload.wikimedia.org/wikipedia/commons/0/04/Port-cros-nature_1.jpg")!,index: 1)
    let cell2 = Image(name: "image 2",url: URL(string: "https://upload.wikimedia.org/wikipedia/en/e/e8/Green_sea_turtle_%28color_correction%29.jpg")!, index: 2)
    let cell3 = Image(name: "image 3", url: URL(string: "https://www.uwlm.ca/wp-content/uploads/2018/04/BigSky.jpg")!, index: 3)
    let cell4 = Image(name: "image 4",url: URL(string: "https://erfanlaashi.com/wp-content/uploads/data/2018/1/19/wallpaperwiki-Bmw-M-Pictures-x-PIC-WPC-PIC-ELW40012904.jpg")!, index: 4)
    let cell5 = Image(name: "image 5",url: URL(string: "https://vignette.wikia.nocookie.net/jamescameronsavatar/images/b/b9/Iknimaya_5_HD.png/revision/latest?cb=20101010194853")!, index: 5)
    let cell6 = Image(name: "image 6",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoCVywjH5uLDiogpt8Go7TcqOHSQhmY04NtkC3sPbsYBobJnFE")!, index: 6)
    let cell7 = Image(name: "image 7",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRekiyCp5tSVrM3_z1Y6TbSkhchARiK6uvQXxPdpCXrzSqC7Am3qw")!, index: 7)
    let cell8 = Image(name: "image 8",url: URL(string: "https://i.pinimg.com/originals/dc/2b/e7/dc2be7e4595e90fedc4785d5d65441bd.jpg")!,index: 8)
    let cell9 = Image(name: "image 9",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9ADFu4DjPVd7WppSl-fqNEBk9MHdcHeJF8Puvf1vhC03UR4NC")!, index: 9)
    let cell10 = Image(name: "image 10",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYjVziPq1QyB1Vre38CBbQO909I6M3FQnzBm0PGBaeiEH_M1Nz")!, index: 10)
    let cell11 = Image(name: "image 11",url: URL(string: "https://cdn-images-1.medium.com/max/2000/1*dT8VX9g8ig6lxmobTRmCiA.jpeg")!, index:11)
    let cell12 = Image(name: "image 12",url: URL(string: "https://upload.wikimedia.org/wikipedia/commons/3/35/Space_Needle_360_Panorama.jpg")!, index: 12)
    let cell13 = Image(name: "image 13",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBPp0EBxwTjz_SWvfH8b4wq5gIYWkgMFuOCsPrsSQA9btgmzEb")!, index: 13)
    let cell14 = Image(name: "image 14", url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTk4xls79xiEDcgnIo0-nfkYJ-9HVLrYbkUJ67PovwU0BkyhN2G")!, index: 14)
    let cell15 = Image(name: "image 15",url: URL(string: "https://i.ytimg.com/vi/1AR-nyny1SU/maxresdefault.jpg")!, index: 15)
    let cell16 = Image(name: "image 16",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNwV1hf65e8VBd-3PlKX1DlKRtdlNTd12CVghtuS4gPYv0dvcb")!, index: 16)
    let cell17 = Image(name: "image 17",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhtT-Rj8289snpemVuDdKH6VojT8_MLHZw9Xsozvidmy2JPC39")!, index: 17)
    let cell18 = Image(name: "image 18",url: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp97z8e4gLLVyfzqr63NzQjJSqk4h63Yu3lqJnZAEoIxpU0xAt")!, index: 18)
    let cell19 = Image(name: "image 19",url: URL(string: "https://upload.wikimedia.org/wikipedia/commons/0/01/Maroc_Marrakech_Majorelle_Luc_Viatour_5.jpg")!, index: 19)
    imageArray.append(cell0);imageArray.append(cell1);imageArray.append(cell2);imageArray.append(cell3);imageArray.append(cell4)
    imageArray.append(cell5);imageArray.append(cell6);imageArray.append(cell7);imageArray.append(cell8);imageArray.append(cell9)
    imageArray.append(cell10);imageArray.append(cell11);imageArray.append(cell12);imageArray.append(cell13);imageArray.append(cell14)
    imageArray.append(cell15);imageArray.append(cell16);imageArray.append(cell17);imageArray.append(cell18);imageArray.append(cell19)
    return imageArray
  }
  
 private func localFilePath(for url: URL) -> URL {
    return documentsPath.appendingPathComponent(url.lastPathComponent)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ImageSegue" {
      if let secondViewController = segue.destination as? SecondViewController {
        secondViewController.image = sender as? UIImage
      }
    }
  }
  
  private func showAlert(_ title: String, _ message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let alertAction = UIAlertAction(title: "OKAY (=", style: .default, handler: nil)
    alert.addAction(alertAction)
    present(alert,animated: true, completion: nil)
  }
}

extension FirstViewController: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return imagesArray.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 140
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
    cell.delegate = self
    let imageObject = imagesArray[indexPath.row]
    cell.setImageObject(imageObject: imageObject, isDownloaded: imageObject.downloaded, download: downloadService.activeDownloads[imageObject.url])
    if let image = downloadedImagesDictionary[imageObject.index] {
      cell.thumbnailView.image = image
    } else
    {
      cell.thumbnailView.image = #imageLiteral(resourceName: "background")
      cell.progressView.progress = 0
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let imageObject = imagesArray[indexPath.row]
    if imageObject.downloaded {
      if let image = downloadedImagesDictionary[imageObject.index] {
        performSegue(withIdentifier: "ImageSegue", sender: image)
      }
    } else {
      showAlert("No Acces to Full Image", "Press Download button!")
    }
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

extension FirstViewController: URLSessionDownloadDelegate, URLSessionDelegate {
  
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
    guard let sourceURL = downloadTask.originalRequest?.url else { return }
    let download = downloadService.activeDownloads[sourceURL]
    downloadService.activeDownloads[sourceURL] = nil
    let destinationURL = localFilePath(for: sourceURL)
    let fileManager = FileManager.default
    try? fileManager.removeItem(at: destinationURL)
    do {
      try fileManager.copyItem(at: location, to: destinationURL)
      download?.image.downloaded = true
    } catch let error {
      print("Failed \(error.localizedDescription)")
    }
    if let index = download?.image.index {
        DispatchQueue.main.async {
          let imageFromURL = try? Data(contentsOf: destinationURL)
          let image = UIImage(data: imageFromURL!)
          self.downloadedImagesDictionary[index] = image
          self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
  }
  
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                  totalBytesExpectedToWrite: Int64) {
    guard let url = downloadTask.originalRequest?.url,
      let download = downloadService.activeDownloads[url]  else { return }
    download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
    DispatchQueue.main.async {
      if let imageCell = self.tableView.cellForRow(at: IndexPath(row: download.image.index,section: 0)) as? ImageCell {
        imageCell.updateProgress(progress: download.progress) }
    }
  }
  
  func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    DispatchQueue.main.async {
      if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
        let completionHandler = appDelegate.backgroundSessionCompletionHandler {
        appDelegate.backgroundSessionCompletionHandler = nil
        completionHandler()}}
  }
}

extension FirstViewController: ImageCellDelegate {
  
  func downloadTapped(_ cell: ImageCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let imageObject = imagesArray[indexPath.row]
      downloadService.startDownload(imageObject)
      reload(indexPath.row)
    }
  }
  
  func pauseTapped(_ cell: ImageCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let imageObject = imagesArray[indexPath.row]
      downloadService.pauseDownload(imageObject)
      reload(indexPath.row)
    }
  }
  
  func resumeTapped(_ cell: ImageCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let imageObject = imagesArray[indexPath.row]
      downloadService.resumeDownload(imageObject)
      reload(indexPath.row)
    }
  }
  
  func reload(_ row: Int) {
    tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
  }
}
