//
//  DownloadService.swift
//  ImgDownloader
//
//  Created by Yurii on 12.05.18.
//  Copyright © 2018 Yurii. All rights reserved.
//

import Foundation

class DownloadService {
  
  var downloadsSession: URLSession!
  
  var activeDownloads = [URL: Download]()
  
  func startDownload(_ image: Image) {
    let download = Download(image: image)
    download.task = downloadsSession.downloadTask(with: image.url)
    download.task!.resume()
    download.isDownloading = true
    activeDownloads[download.image.url] = download
  }
  
  func pauseDownload(_ image: Image) {
    guard let download = activeDownloads[image.url] else { return }
    if download.isDownloading {
      download.task?.cancel(byProducingResumeData: { data in
        download.resumeData = data
      })
      download.isDownloading = false
    }
  }
  
  func resumeDownload(_ image: Image) {
    guard let download = activeDownloads[image.url] else { return }
    if let resumeData = download.resumeData {
      download.task = downloadsSession.downloadTask(withResumeData: resumeData)
    } else {
      download.task = downloadsSession.downloadTask(with: download.image.url)
    }
    download.task!.resume()
    download.isDownloading = true
  }
}
