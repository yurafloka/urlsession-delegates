//
//  ImageCell.swift
//  ImgDownloader
//
//  Created by Yurii on 12.05.18.
//  Copyright © 2018 Yurii. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

  weak var delegate: ImageCellDelegate?
  
  @IBOutlet weak var thumbnailView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var progressView: UIProgressView!
  @IBOutlet weak var progressLabel: UILabel!
  @IBOutlet weak var pauseButton: UIButton!
  @IBOutlet weak var downloadButton: UIButton!
  
  @IBAction func pauseOrResumeBtnTapped(_ sender: AnyObject) {
    if(pauseButton.titleLabel!.text == "Pause") {
      delegate?.pauseTapped(self)
    } else {
      delegate?.resumeTapped(self)
    }
  }
  
  @IBAction func downloadBtnTapped(_ sender: AnyObject) {
    delegate?.downloadTapped(self)
  }
  
  func updateProgress(progress: Float) {
    progressView.progress = progress
    progressLabel.text = String("\(Int(progress*100))%")
  }
  
  func setImageObject(imageObject: Image, isDownloaded: Bool, download: Download?) {
    titleLabel.text = imageObject.name
    
    var downloadIsRunning = false
    
    if let download = download {
      downloadIsRunning = true
      let title = download.isDownloading ? "Pause" : "Resume"
      pauseButton.setTitle(title, for: .normal)
      progressLabel.text = download.isDownloading ? "" : "Download paused"
    }
    
    pauseButton.isHidden = !downloadIsRunning
    progressView.isHidden = !downloadIsRunning
    progressLabel.isHidden = !downloadIsRunning
    downloadButton.isHidden = isDownloaded || downloadIsRunning
  }
}
