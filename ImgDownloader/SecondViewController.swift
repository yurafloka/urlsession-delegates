//
//  SecondViewController.swift
//  ImgDownloader
//
//  Created by Yurii Tsymbala on 12.05.18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

  @IBOutlet weak var bigThumbnail: UIImageView!
  
  var image:UIImage?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    bigThumbnail.image = image
  }
}
